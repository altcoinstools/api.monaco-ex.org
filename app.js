/*
 * Copyright (C) 2014 MonacoEx Project. All rights reserved.
 *
 * Licensed under AGPL.
 *
 * We provide the another (not AGPL) license.
 * Please ask admin@monaco-ex.org for more details.
 */
var crawler = require('./crawler.js');
var crawler_etwings = require('./crawler-etwings.js');
var coind = require('./stat-coind.js');

var restify = require('restify');
var async = require('async');

var server = restify.createServer({
    formatters: {
	'application/json': function formatJSON(req, res, body) {
	    if (body instanceof Error) {
		return JSON.stringify({ 'error': body.body });
	    }
	    if (req.params.format == 'jsonp') {
		var callback = req.query.callback || 'callback';
		return callback + '(' + JSON.stringify(body) + ');';
	    }
	    return JSON.stringify(body);
	},
	'text/html': function formatHTML(req, res, body) {
	    if (body instanceof Error) {
		return JSON.stringify(body);
	    }
	    var md = require("marked");
	    return md(body);
	}
    }
});
server.use(restify.queryParser());

//-----
server.get( '/v0', document_v0);
server.head('/v0', document_v0);

function document_v0(req, res, next)
{
    res.setHeader('content-type', 'text/html');
    var fs = require("fs");
    res.send(fs.readFileSync('./api-doc/v0/index.md', 'utf-8'));
}

//-----
server.get( '/v0/moving_average/:pair/:format', moving_average);
server.head('/v0/moving_average/:pair/:format', moving_average);

function moving_average(req, res, next) {
    res.setHeader('content-type', 'application/json');
    if (req.params.format != 'json' &&
	req.params.format != 'jsonp') {
	res.send({'error': 'Unsupported format: ' + req.params.type});
    }

    if (req.params.pair == 'mona_jpy') {
        async.parallel([
            function(callback) {
                crawler_etwings.calcMovingAverage(callback,  1);
            },
            function(callback) {
                crawler_etwings.calcMovingAverage(callback,  3);
            },
            function(callback) {
                crawler_etwings.calcMovingAverage(callback,  7);
            },
            function(callback) {
                crawler_etwings.calcMovingAverage(callback, 14);
            },
            function(callback) {
                crawler_etwings.calcMovingAverage(callback, 28);
            }
        ], function(err, results) {
            if (err) {
                throw err;
            }
            res.send(results);
        }); 
    } else {
        res.send([]);
    }
}

//----
server.get( '/v0/currencies/:format', currencies);
server.head('/v0/currencies/:format', currencies);

function currencies(req, res, next) {
    res.setHeader('content-type', 'application/json');
    if (req.params.format != 'json' &&
	req.params.format != 'jsonp') {
	res.send({'error': 'Unsupported format: ' + req.params.type});
    }

    res.send({ 'result': coind.getCurrencies() });
}

//----
server.get( '/v0/difficulty/:currency/:format', difficulty);
server.head('/v0/difficulty/:currency/:format', difficulty);

function difficulty(req, res, next) {
    var max_count = 15;

    res.setHeader('content-type', 'application/json');
    if (req.params.format != 'json' &&
	req.params.format != 'jsonp') {
	res.send({'error': 'Unsupported format: ' + req.params.type});
    }

    var count_ = req.query.count || max_count;
    count = parseInt(count_);
    if (!isFinite(count) || count < 0) {
	res.send({'error': "Invalid value " + count_ + " for 'count'."});
    } else if (count > max_count) {
	res.send({'error': "You can't get more than " +
		  max_count + " items. Please consider to subscribe the standard contract."});
    } else {
	coind.recentDifficulty(req.params.currency,
			       count,
			       function(err, result) {
				   var response;
				   if (!err) {
				       response = {'result': result};
				   } else {
				       response = {'error': err};
				   }

				   res.send(response);
			       });
    }
}

//----
server.get( '/v0/networkhashps/:currency/:format', nethashps);
server.head('/v0/networkhashps/:currency/:format', nethashps);

function nethashps(req, res, next) {
    var max_count = 15;

    res.setHeader('content-type', 'application/json');
    if (req.params.format != 'json' &&
	req.params.format != 'jsonp') {
	res.send({'error': 'Unsupported format: ' + req.params.type});
    }

    var count_ = req.query.count || max_count;
    count = parseInt(count_);
    if (!isFinite(count) || count < 0) {
	res.send({'error': "Invalid value " + count_ + " for 'count'."});
    } else if (count > max_count) {
	res.send({'error': "You can't get more than " +
		  max_count + " items. Please consider to subscribe the standard contract."});
    } else {
	coind.recentNetHashPs(req.params.currency,
			       count,
			       function(err, result) {
				   var response;
				   if (!err) {
				       response = {'result': result};
				   } else {
				       response = {'error': err};
				   }

				   res.send(response);
			       });
    }
}

//----

crawler.daemon();
crawler_etwings.daemon();
coind.daemon();

server.listen(process.env.PORT || 8080, function() {
    console.log('%s listening at %s', server.name, server.url);
});
