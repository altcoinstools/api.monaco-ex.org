/*
 * Copyright (C) 2014 MonacoEx Project. All rights reserved.
 *
 * Licensed under AGPL.
 *
 * We provide the another (not AGPL) license.
 * Please ask admin@monaco-ex.org for more details.
 */
var sql = require('mssql'); 
var request = require('request');
var url = require('url');

var NAME = 'Clawler-etwings';

var db_url = url.parse(process.env.DB_URL);

var config = {
    user: db_url.auth.split(':')[0],
    password: db_url.auth.split(':')[1],
    server: db_url.hostname,
    database: db_url.pathname.split('/')[1],
    options: {
        encrypt: true // Use this if you're on Windows Azure
    }
}

function store_to_db(item) {
    var connection = new sql.Connection(config, function(err) {
        if (!err) {
            select_string = 'SELECT date FROM Etwings WHERE ' +
                'tid=' + item.tid + ' AND ' + 'date=' + item.date;
            var select_request = new sql.Request(connection); // or: var request = connection.request();
            select_request.query(select_string, function(err, recordset) {
		if (err) {
		    console.log(NAME, err);
                } else if (recordset.length != 0) {
//		    console.log(NAME, 'Record already exists. Skip.');
		} else {
                    console.log(NAME, 'New trade found.');
                    insert_string = 'INSERT INTO Etwings (date, price, amount, tid, currency_pair, trade_type) VALUES (' +
			item.date + ', ' +
                        item.price + ', ' +
                        item.amount + ', ' +
			item.tid + ', ' +
			"'" + item.currency_pair + "'," +
                        "'" + item.trade_type + "')";
                    var insert_request = new sql.Request(connection);
                    insert_request.query(insert_string, function(err, recordset) {
			if (err) console.log(NAME, err);
                    });
		}
            });
        }
    });
}

var url = 'https://exchange.etwings.com/api/1/trades/mona_jpy';

function daemon() {
    console.log(NAME, 'Crawler stated.');
    request.get(
        { url: url, json: true },
        function (err, response, items) {
            if (!err && response.statusCode === 200) {
		console.log(NAME, 'Starging store_to_db.');
                for (var i = 0; i < items.length; i++) {
                   store_to_db(items[i]);
                };
            } else {
		console.log(NAME, 'something wrong in get.');
	    }
        }
    );
}

function calcMovingAverage(callback, days) {
    var epochDate = (+new Date()) / 1000;
    var select_string = 'SELECT AVG(price) As Average FROM Etwings WHERE Date > ';
    select_string += (epochDate - 43200 * days).toFixed();

    var connection = new sql.Connection(config, function(err) {
        if (err) {
          console.log(NAME, err);
        }
        var select_request = new sql.Request(connection);
        select_request.query(select_string, function(err, recordset) {
            var price = -1;
            if (!err && recordset.length == 1) {
		price = recordset[0].Average;
            }
            callback(err, { days: days, price: price });
        });
    });
}

//----

setInterval(daemon, 12 * 60 * 60 * 1000); /* every 12 hours. */

//----

module.exports = {
    daemon: daemon,
    calcMovingAverage: calcMovingAverage
};

