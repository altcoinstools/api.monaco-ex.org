/*
 * Copyright (C) 2014 MonacoEx Project. All rights reserved.
 *
 * Licensed under AGPL.
 *
 * We provide the another (not AGPL) license.
 * Please ask admin@monaco-ex.org for more details.
 */
var bitcoin = require('bitcoin');
var redis =  require('redis');
var async = require('async');

var rpcports = {
  bitcoin: 8332,
  dogecoin: 22555,
  fujicoin: 3776,
  kumacoin: 7585,
  likecoin: 55883,
  monacoin: 9402,
  ringo: 9292,
  sakuracoin: 9302,
  sayacoin: 8332,
  sha1coin: 9512,
  yaycoin: 8383
};

//----

function getKeyForDifficulty(currency) {
    return currency + ':difficulty';
}

function getKeyForNetHashPs(currency) {
    return currency + ':networkhashps';
}

//----

function getClient() {
    var client = redis.createClient(null, process.env.REDIS_HOST || null);
    process.env.REDIS_PASS && client.auth(process.env.REDIS_PASS);
    return client;
}

function daemon() {
    for (var currency in rpcports) {
	// all config options are optional
	async.waterfall([
	    function (callback) {
		var client = getClient();
		var wallet = new bitcoin.Client({
		    host: 'localhost',
		    port: rpcports[currency],
		    user: process.env.WALLET_RPC_USER,
		    pass: process.env.WALLET_RPC_PASS,
		    timeout: 30000
		});
		callback(null, client, wallet);
	    },
	    function (client, wallet, callback) {
		wallet.getDifficulty(function(err, difficulty, resHeader) {
		    if (err) return console.log(err);
		    currency = resHeader.server.split('-')[0].toLowerCase();
		    client.lpush(getKeyForDifficulty(currency), [ (+new Date), JSON.stringify(difficulty) ],
				 function(err, result) {
				     if (err) console.log('lpush difficulty:', err);
				     callback(null, client, wallet);
				 });
		});
	    },
	    function (client, wallet, callback) {
		wallet.getMiningInfo(function(err, hashps, resHeader) {
		    if (err) return console.log(err);
		    currency = resHeader.server.split('-')[0].toLowerCase();
		    client.lpush(getKeyForNetHashPs(currency), [ (+new Date), JSON.stringify(hashps['networkhashps']) ],
				 function(err, result) {
				     if (err) console.log('lpush nethashps:', err);
				     callback(null, client, wallet);
				 });
		});
	    }], function (err, client, wallet) {
		if (err) {
//		    throw err;
		}
		client.quit();
	    });
    }
}


//----

function recentDifficulty(currency, len, callback) {
    if (!rpcports[currency]) {
	callback(null, { error: 'Unknown currency.' });
    }
    var client = getClient();
    client.lrange(getKeyForDifficulty(currency), 0, len - 1,
		  function(err, r) {
		      if (!err) {
			  if (callback) {
			      var result = r.map(function(item) { return JSON.parse("[" + item + "]"); });
			      callback(err, result);
			  }
		      } else {
			  console.log('', err);
			  callback(err, []);
		      }
		      client.quit();
		  });
}

function recentNetHashPs(currency, len, callback) {
    if (!rpcports[currency]) {
	callback(null, { error: 'Unknown currency.' });
    }
    var client = getClient();
    client.lrange(getKeyForNetHashPs(currency), 0, len - 1,
		  function(err, r) {
		      if (!err) {
			  if (callback) {
			      var result = r.map(function(item) { return JSON.parse("[" + item + "]"); });
			      callback(err, result);
			  }
		      } else {
			  console.log('', err);
			  callback(err, []);
		      }
		      client.quit();
		  });
}

function getCurrencies() {
    return Object.keys(rpcports);
}

//----

setInterval(daemon, 10 * 60 * 1000); /* 10 min. */

//----
module.exports = {
    daemon: daemon,
    recentDifficulty: recentDifficulty,
    recentNetHashPs: recentNetHashPs,
    getCurrencies: getCurrencies
};

