
API reference for api.monaco-ex.org
===================================

This document is based on API version 0.

General
-------

All API may return `{ 'error': Object }`.

Currency List
-------------

### API

```
http://api.monaco-ex.org/v0/currencies/:format[?callback=:callback]
```

### Params

* `:format` == `(json|jsonp)`
* `callback` is only effective when :format == `jsonp`. `callback` is used as a default function name if you don't specify by the query string.

### Result

Array of currency names.

### Example

http://api.monaco-ex.org/v0/currencies/json

Difficulty History
------------------

### API

```
http://api.monaco-ex.org/v0/difficulty/:currency/:format[?callback=:callback]&[count=:count]
```

### Params

* `:currenty` is a currency name. The list of available currencies can be got by the currency list API.
* `:format` == `(json|jsonp)`
* The query parameter `callback` is only effective when :format == `jsonp`. `callback` is used as a default function name if you don't specify by the query string.
* `:count` must be 0 and positive integer less than 16.

### Result

`[[unixtime1, difficulty1], [unixtime2, difficulty2], ....]`

NOTE: It's possible that the array length of the result is shorter than `:count`.

### Example

http://api.monaco-ex.org/v0/difficulty/bitcoin/jsonp?callback=foobar&count=3

Network Hash-rate
-----------------

### API

```
http://api.monaco-ex.org/v0/networkhashps/:currency/:format[?callback=:callback]&[count=:count]
```

### Params

* `:currenty` is a currency name. The list of available currencies can be got by the currency list API.
* `:format` == `(json|jsonp)`
* The query parameter `callback` is only effective when :format == `jsonp`. `callback` is used as a default function name if you don't specify by the query string.
* `:count` must be 0 and positive integer less than 16.

### Result

`[[unixtime1, networkhashps], [unixtime2, networkhashps], ....]`

NOTE: It's possible that the array length of the result is shorter than `:count`.

### Example

http://api.monaco-ex.org/v0/networkhashps/bitcoin/jsonp?callback=foobar&count=3

### NOTE

In some currencies, values may be unreliable.
This API is not recommended to use for mining or merchant applications.

Moving Average
--------------

### API

```
http://api.monaco-ex.org/v0/moving_average/:currency_pair/:format[?callback=:callback]
```

### Params

* `:currenty_pair` is a name of currency pair. Only `mona_jpy` is supported.
* `:format` == `(json|jsonp)`
* The query parameter `callback` is only effective when :format == `jsonp`. `callback` is used as a default function name if you don't specify by the query string.

### Result

`[[unixtime1, networkhashps], [unixtime2, networkhashps], ....]`

NOTE: It's possible that the array length of the result is shorter than `:count`.

### Example

http://api.monaco-ex.org/v0/moving_average/mona_jpy/jsonp?callback=foobar

### NOTE

In some currencies, values may be unreliable.
This API is not recommended to use for merchant applications.
