/*
 * Copyright (C) 2014 MonacoEx Project. All rights reserved.
 *
 * Licensed under AGPL.
 *
 * We provide the another (not AGPL) license.
 * Please ask admin@monaco-ex.org for more details.
 */
var sql = require('mssql'); 
var request = require('request');
var url = require('url');

db_url = url.parse(process.env.DB_URL);

var config = {
    user: db_url.auth.split(':')[0],
    password: db_url.auth.split(':')[1],
    server: db_url.hostname,
    database: db_url.pathname.split('/')[1],
    options: {
        encrypt: true // Use this if you're on Windows Azure
    }
}

function store_to_db(item) {
    var connection = new sql.Connection(config, function(err) {
        if (!err) {
            select_string = 'SELECT time FROM monax WHERE ' +
                'Price=' + item.price + ' AND ' +
                'Amount=' + item.amount + ' AND ' +
                'Time=' + item.time + ' AND ' +
                "OrderType='" + item.order + "'";
            var select_request = new sql.Request(connection); // or: var request = connection.request();
            select_request.query(select_string, function(err, recordset) {
		if (err) {
		    console.log(err);
                } else if (recordset.length != 0) {
//		    console.log('Record already exists. Skip.');
		} else {
                    console.log('New trade found.');
                    insert_string = 'INSERT INTO Monax (Price, Amount, Time, OrderType) VALUES (' +
                        item.price + ', ' +
                        item.amount + ', ' +
                        item.time + ', ' +
                        "'" + item.order + "')";
                    var insert_request = new sql.Request(connection);
                    insert_request.query(insert_string, function(err, recordset) {
			if (err) console.log(err);
                    });
		}
            });
        }
    });
}

var url = 'https://monax.jp/api/historymncjpyv1';

function daemon() {
    console.log('Crawler stated.');
    request.get(
        { url: url, json: true },
        function (err, response, items) {
            if (!err && response.statusCode === 200) {
		console.log('Starging store_to_db.');
                for (var i = 0; i < items.length; i++) {
                   store_to_db(items[i]);
                };
            } else {
		console.log('something wrong in get.');
	    }
        }
    );
}

function calc_movage_monax(callback, days) {
    var epochDate = new Date().valueOf() / 1000;
    var select_string = 'SELECT AVG(price) As Average FROM monax WHERE Time > ';
    select_string += (epochDate - 43200 * days).toFixed();

    var connection = new sql.Connection(config, function(err) {
        if (err) {
          console.log(err);
        }
        var select_request = new sql.Request(connection);
        select_request.query(select_string, function(err, recordset) {
            var price = -1;
            if (!err && recordset.length == 1) {
		price = recordset[0].Average;
            }
            callback(null, { days: days, price: price });
        });
    });
}

//----

setInterval(daemon, 60 * 60 * 1000); /* every an hour. */

//----

module.exports = {
    daemon: daemon,
    calc_movave_monax: calc_movage_monax
};

